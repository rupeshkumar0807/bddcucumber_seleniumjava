Feature: Login functionality

Scenario: verify and validate the functionality of login with valid details.
Given when user navigates to login page
When user entered valid email address "rupesh.surya62@gmail.com"
And user entered valid password "Test@123"
And user click on login button 
Then user should be able to login successfully

Scenario: verify and validate the functionality of login with Invalid details.
Given when user navigates to login page
When user entered valid email address "rupeshsurya62@gmail.com"
And user entered valid password "Test@123"
And user click on login button 
Then user should not be able to login successfully

Scenario: verify and validate the functionality of login without entering details.
Given when user navigates to login page
When user will not enter valid email address 
And user will not enter valid password
And user click on login button 
Then user should not be able to login successfully